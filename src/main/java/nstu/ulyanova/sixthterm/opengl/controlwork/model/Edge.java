package nstu.ulyanova.sixthterm.opengl.controlwork.model;

// Класс для представления грани
public class Edge {
    private int maxy;
    private float x;
    private float m1;
    private float z;
    private float m1z;
    private Edge next;

    public Edge() {
    }

    public Edge(int maxy, float x, float m1, float z, float m1z) {
        this.maxy = maxy;
        this.x = x;
        this.m1 = m1;
        this.z = z;
        this.m1z = m1z;
        this.next = null;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public Edge getNext() {
        return next;
    }

    public void setNext(Edge next) {
        this.next = next;
    }

    public int getMaxy() {
        return maxy;
    }

    public void setMaxy(int maxy) {
        this.maxy = maxy;
    }

    public float getM1() {
        return m1;
    }

    public void setM1(float m1) {
        this.m1 = m1;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public float getM1z() {
        return m1z;
    }

    public void setM1z(float m1z) {
        this.m1z = m1z;
    }
}
