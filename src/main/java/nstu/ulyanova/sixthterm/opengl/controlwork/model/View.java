package nstu.ulyanova.sixthterm.opengl.controlwork.model;

// Вид камеры
public class View {
    private float a;  // поворот по горизонтали
    private float b;  // поворот по вертикали относительно верха
    private float zk; // дистанция камеры
    private float zp; // дистанция плоскости проекции

    public View() {
    }

    public View(float a, float b, float zk, float zp) {
        this.a = a;
        this.b = b;
        this.zk = zk;
        this.zp = zp;
    }

    // Поворот камеры
    public void rotate(float a, float b) {
        this.a = a;
        this.b = b;
    }

    // Поворот камеры
    public Vector3d getCameraPoint() {
        Vector3d v = new Vector3d(0, 0, zk);
        v.rotate(-a, -b);
        return v;
    }

    public float getA() {
        return a;
    }

    public float getB() {
        return b;
    }

    public float getZk() {
        return zk;
    }

    public float getZp() {
        return zp;
    }
}
