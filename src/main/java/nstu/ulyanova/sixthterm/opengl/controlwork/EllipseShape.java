package nstu.ulyanova.sixthterm.opengl.controlwork;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import nstu.ulyanova.sixthterm.opengl.controlwork.model.Edge;
import nstu.ulyanova.sixthterm.opengl.controlwork.model.Polygon;
import nstu.ulyanova.sixthterm.opengl.controlwork.model.Vector3d;
import nstu.ulyanova.sixthterm.opengl.controlwork.model.View;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;
import static nstu.ulyanova.sixthterm.opengl.controlwork.Utils.*;

public class EllipseShape implements GLEventListener, KeyListener {
    public static final float NINETY_DEGREES = 90.f;
    public static final float THREE_HUNDRED_SIXTY_DEGREES = 360.f;
    private GL2 gl;
    int scene = '2';
    //    float mouseX = 0;
//    float mouseY = 0;
//    float mouseXscreen = 0;
//    float mouseYscreen = 0;
    float cameraVerticalAngle = -45.f;
    float cameraHorizontalAngle = 0.f;
    float objectX = 0.f;
    float objectY = 0.f;
    float objectSize = 0.5f;
    float objectStep = 12.f;
    float manipulationState = 0; // Режим перемещения
    int screenWidth = 300;
    int screenHeight = 300;

    //Полигоны
    List<Polygon> polygons;

    // Задание параметров света
    Vector3d lightIa = new Vector3d(0.8f, 0.4f, 0.4f);  // Интенсивность фонового освещения
    Vector3d lightId = new Vector3d(0.5f, 0.5f, 0.9f);  // Интенсивность источника света
    Vector3d lightIs = new Vector3d(0.5f, 0.5f, 0.9f);  // Интенсивность источника света
    Vector3d spherePa = new Vector3d(0.5f, 0.5f, 0.5f); // Коэффициант фонового отражения
    Vector3d spherePd = new Vector3d(0.5f, 0.8f, 0.2f); // Коэффициант диффузного отражения
    Vector3d spherePs = new Vector3d(0.0f, 0.0f, 0.6f); // Коэффициант зеркального отражения
    Vector3d light = new Vector3d(2.0f, -2.0f, 2.f);    // Определяем источник света

    // Преобразование из сферических в декартовы координаты шара
    Vector3d sphericToDecart(float R, float B, float L) {
        return new Vector3d(
                R * cosd(B) * sind(L),
                R * cosd(B) * cosd(L),
                R * sind(B));
    }

    // Преобразование из сферических в декартовы координаты объекта по варианту
    Vector3d mySphericToDecart(float R, float B, float L) {
        Vector3d p = new Vector3d(
                R * cosd(B) * sind(L),
                R * cosd(B) * cosd(L),
                R * 2 * sind(B));
        return p;
    }

    // Получить полигон из точки на сфере
    Polygon polygonFromMySpheric(float r, float dB, float dL, float b, float l) {
        Polygon p = new Polygon();
        p.setVector(mySphericToDecart(r, b, l), 0);
        p.setVector(mySphericToDecart(r, b + dB, l), 1);
        p.setVector(mySphericToDecart(r, b + dB, l + dL), 2);
        p.setVector(mySphericToDecart(r, b, l + dL), 3);
        return p;
    }

    // Рисовка линии
    void drawLine(Vector3d p) {
        gl.glVertex2f(p.getX(), p.getY());
    }

    // Отрисовка линии в буфер с глубиной
    void drawLineToBuffer(Vector3d v1, Vector3d v2, float[] buffer) {
        gl.glPointSize(1.0f);
        gl.glBegin(GL_POINTS);
        for (float i = 0.f; i <= 1.f; i += 0.01f) {
            int x = (int) (v1.getX() + (v2.getX() - v1.getX()) * i);
            int y = (int) (v1.getY() + (v2.getY() - v1.getY()) * i);
            float z = v1.getZ() + (v2.getZ() - v1.getZ()) * i;
            int pointIndex = y * screenHeight * 4 + x * 4;
            if (buffer[pointIndex + 3] < z) {
                buffer[pointIndex + 0] = 1.f;
                buffer[pointIndex + 1] = 1.f;
                buffer[pointIndex + 2] = 1.f;
                buffer[pointIndex + 3] = z;
            }
        }
        gl.glEnd();
    }

    // Калбек на события клавиатуры
//    void keyboard(unsigned char key, int x, int y) {
//        //mouseX = x;
//        //mouseY = y;
//        if (key == '\x1B') {                 // Escape - выход
//            exit(EXIT_SUCCESS);
//        }
//        else if (key >= '1' && key <= '9') {
//            scene = key - '0';
//        }
//        else if (key == 'e') {
//            manipulationState = 0;
//        }
//        else if (key == 'r') {
//            manipulationState = 1;
//        }
//        else if (key == 't') {
//            manipulationState = 2;
//        }
//        else if (key == 'w') {
//            if (manipulationState == 0)
//                cameraVerticalAngle += 5;
//            else if (manipulationState == 1)
//                objectY += 0.1;
//        }
//        else if (key == 's') {
//            if (manipulationState == 0)
//                cameraVerticalAngle -= 5;
//            else if (manipulationState == 1)
//                objectY -= 0.1;
//        }
//        else if (key == 'a') {
//            if (manipulationState == 0)
//                cameraHorizontalAngle -= 5;
//            else if (manipulationState == 1)
//                objectX -= 0.1;
//        }
//        else if (key == 'd') {
//            if (manipulationState == 0)
//                cameraHorizontalAngle += 5;
//            else if (manipulationState == 1)
//                objectX += 0.1;
//        }
//        else if (key == 'o') {
//            spherePa = Vector3d(randomFloat(0.f, 1.f), randomFloat(0.f, 1.f), randomFloat(0.f, 1.f));
//            spherePd = Vector3d(randomFloat(0.f, 1.f), randomFloat(0.f, 1.f), randomFloat(0.f, 1.f));
//            spherePs = Vector3d(randomFloat(0.f, 1.f), randomFloat(0.f, 1.f), randomFloat(0.f, 1.f));
//        }
//        else if (key == 'l') {
//            lightIa = Vector3d(randomFloat(0.f, 1.f), randomFloat(0.f, 1.f), randomFloat(0.f, 1.f));
//            lightId = Vector3d(randomFloat(0.f, 1.f), randomFloat(0.f, 1.f), randomFloat(0.f, 1.f));
//            lightIs = Vector3d(randomFloat(0.f, 1.f), randomFloat(0.f, 1.f), randomFloat(0.f, 1.f));
//        }
//        else if (key == 'n') {
//            objectSize *= 0.9;
//        }
//        else if (key == 'm') {
//            objectSize *= 1.1;
//        }
//        else if (key == 'h') {
//            objectStep *= 0.9;
//        }
//        else if (key == 'j') {
//            objectStep *= 1.1;
//        };
//        display();
//    }

    // Движение мыши (калбек)
//    void mouse(int x, int y) {
//        mouseXscreen = x;
//        mouseYscreen = screenHeight - y;
//        mouseX = static_cast <float> (x) / screenWidth * 2 - 1;
//        mouseY = 1 - static_cast <float> (y) / screenHeight * 2;
//
//        light = Vector3d(mouseX, mouseY, 2.f);
//
//        display();
//    }

    // Создание фигуры по варианту
    List<Polygon> generateFigure(float R, float dB, float dL) {
        List<Polygon> polygons = new ArrayList<>();

        float B, L; // Текущие широта и долгота
        for (B = -NINETY_DEGREES; B < NINETY_DEGREES; B += dB) {
            for (L = 0.f; L < THREE_HUNDRED_SIXTY_DEGREES; L += dL) {
                Polygon polygon = polygonFromMySpheric(R, dB, dL, B, L);
                // Вычисление центра нормали
                Vector3d p = mySphericToDecart(R, B + dB / 2, L + dL / 2);

                // Вычисляем нормаль к поверхности в точке p
                Vector3d m = p;
                if (B > 0)
                    m = new Vector3d(0, 0, R).substact(p);
                m.normalize();
                polygon.setNormal(m);
                polygons.add(polygon);
            }
        }
        return polygons;
    }

    // Функция вычисления освещения
    Vector3d calculateColor(Polygon polygon, View view, Vector3d light) {
        // Вычисляем центр нормали;
        Vector3d p = polygon.center();
        // Вычисляем вектор нормали
        Vector3d m = polygon.getNormal();
        // Вычисляем вектор vectors, соединяющий точку p с глазом наблюдателя;
        Vector3d cam = view.getCameraPoint();
        Vector3d v = cam.substact(p);
        v.normalize();
        // Вычисляем вектор s, соединяющий точку p с источником света.
        Vector3d s = light.substact(p);
        s.normalize();
        // Определяем цвет
        Vector3d h = s.add(v);
        h.normalize();
        // Определяем составляющую по Ламберту
        float lambert = max(0, s.multiply(m) / (s.length() * m.length())); // diffuse reflections
        // Определяем составляющую по Фонгу
        float phong = max(0, h.multiply(m) / (h.length() * m.length())); // specular reflections
        // Определяем цвет смешивая все составляющие и фоновое освещение
        Vector3d finalI = new Vector3d();
        finalI.setX(lightIa.getX() * spherePa.getX() + lightId.getX() * spherePd.getX() * lambert + lightIs.getX() * spherePs.getX() * phong);
        finalI.setY(lightIa.getY() * spherePa.getY() + lightId.getY() * spherePd.getY() * lambert + lightIs.getY() * spherePs.getY() * phong);
        finalI.setZ(lightIa.getZ() * spherePa.getZ() + lightId.getZ() * spherePd.getZ() * lambert + lightIs.getZ() * spherePs.getZ() * phong);
        return finalI;
    }

    // Отрисовка каркасная
    void drawWiredObject(View view) {
        for (int i = 0; i < polygons.size(); i++) {
            Polygon polygon = polygons.get(i);
            polygon.translate(new Vector3d(objectX, objectY));

            // Преобразование в перспективу и поворачиваем
            polygon.rotate(view.getA(), view.getB());
            polygon.withPerspective(view.getZk(), view.getZp());
            // Рисование полигона
            gl.glBegin(GL_LINE_LOOP);
            drawLine(polygon.getVector(0));
            drawLine(polygon.getVector(1));
            drawLine(polygon.getVector(2));
            drawLine(polygon.getVector(3));
            gl.glEnd();
        }

    }

    // Отрисовка объекта линиями (с удадением невидимых)
    void drawSolidWiredObject(View view) {
        float[] buffer = new float[screenWidth * screenHeight * 4];
        Arrays.fill(buffer, 0.f);

        // Проходимся по всем полигонам и рисуем глубину
        for (int i = 0; i < polygons.size(); i++) {
            Polygon polygon = polygons.get(i);
            polygon.translate(new Vector3d(objectX, objectY));

            // Цвета не важен, только глубина
            Vector3d color = new Vector3d(0.f, 0.f, 0.f);

            // Преобразование в перспективу и поворачиваем
            polygon.rotate(view.getA(), view.getB());
            polygon.withPerspective(view.getZk(), view.getZp());
            // Рисование полигона
            drawPolygon(polygon, color, buffer);
        }
        // Проходимся по всем линиям и рисуем их
        for (int i = 0; i < polygons.size(); i++) {
            Polygon polygon = polygons.get(i);
            polygon.translate(new Vector3d(objectX, objectY));
            polygon.rotate(view.getA(), view.getB());
            polygon.withPerspective(view.getZk(), view.getZp());

            int points = 4;
            // Преобразование к пикселям

            boolean stop = false;
            for (int y = 0; y < points; y++) {
                Vector3d vectors = polygon.getVector(y);
                vectors.setX((vectors.getX() * 0.5f + .5f) * (float) screenWidth);
                vectors.setY((vectors.getY() * 0.5f + .5f) * (float) screenHeight);
                vectors.setZ(vectors.getZ() + 1000.f + 0.02f); // Хак чтобы рисовалось поверх

                if (vectors.getX() >= screenWidth || vectors.getX() < 0 || vectors.getY() >= screenHeight || vectors.getY() < 0) {
                    stop = true;
                    break;
                }
            }
            if (stop) continue;
            // Заполняем буфер линиями
            drawLineToBuffer(polygon.getVector(0), polygon.getVector(1), buffer);
            drawLineToBuffer(polygon.getVector(1), polygon.getVector(2), buffer);
            drawLineToBuffer(polygon.getVector(2), polygon.getVector(3), buffer);
            drawLineToBuffer(polygon.getVector(3), polygon.getVector(0), buffer);
        }


        drawBuffer(buffer);
    }

    // Отрисовка освещённого объекта
    void drawSolidObject(View view) {
        float[] buffer = new float[screenWidth * screenHeight * 4]; // заполнение нулями

        int polygonsCount = polygons.size();
        // Проходимся по всем полигонам
        for (int i = 0; i < polygonsCount; i++) {
            Polygon polygon = polygons.get(i);
            polygon.translate(new Vector3d(objectX, objectY));

            Vector3d color = calculateColor(polygon, view, light);

            // Преобразование в перспективу и поворачиваем
            polygon.rotate(view.getA(), view.getB());
            polygon.withPerspective(view.getZk(), view.getZp());
            // Рисование полигона
            drawPolygon(polygon, color, buffer);
        }

        drawBuffer(buffer);
    }


    // Добавить грань в ТАР с сохранением порядка
    Edge tapInsertEdge(Edge tap, Edge edge) {
        edge.setNext(null);

        // Если всего 0
        if (tap == null) return edge;
        else {

            Edge prev = tap;
            Edge curr = prev.getNext();
            if (curr == null) { // Если всего 1
                if (edge.getX() < prev.getX()) {
                    tap = edge;
                    edge.setNext(prev);
                } else {
                    tap.setNext(edge);
                }
            } else {  // Если больше
                while (true) {
                    if (curr == null || edge.getX() < curr.getX()) {
                        prev.setNext(edge);
                        edge.setNext(curr);
                        break;
                    }
                    prev = curr;
                    curr = prev.getNext();
                }
            }
        }
        return tap;
    }

    // Отсортировать ТАР
    Edge tapSort(Edge tap) {
        Edge edge = tap, nexte;
        //tap = null;
        while (edge != null) {
            nexte = edge.getNext();
            tap = tapInsertEdge(tap, edge);
            edge = nexte;
        }
        return tap;
    }

    // Очистить ТАР
    void tapClear(Edge tap) {
        for (Edge e = tap; e != null; ) {
            Edge nexte = e.getNext();
            e.setNext(null);
            e = nexte;
        }
    }

    // Очистить ТР
    void TPClear(Edge[] TP, int height) {
        for (int i = 0; i < height; i++) {
            Edge e = TP[i];
            if (e == null) continue;
            Edge nexte;
            do {
                nexte = e.getNext();
                e.setNext(null);
                e = nexte;
            } while (e != null);
            TP[i] = null;
        }
    }

    // Отрисовать буфер на экран точками
    void drawBuffer(float[] buffer) {
        int bufferIndex = 0;
        gl.glPointSize(1.0f);
        gl.glBegin(GL_POINTS);
        for (int y = 0; y < screenHeight; y++) {
            for (int x = 0; x < screenWidth; x++) {
                if (buffer[bufferIndex + 2] != 0.f) { // Не рисовать фон
                    gl.glColor3f(buffer[bufferIndex], buffer[bufferIndex + 1], buffer[bufferIndex + 2]);
                    gl.glVertex2i(x, y);
                }
                bufferIndex += 4; // d
            }
        }
        gl.glEnd();
        gl.glFlush();
    }


    // Рисовка полигона (сканированием линий)
    void drawPolygon(Polygon p, Vector3d lightI, float[] buffer) {
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();
//        gl.gluOrtho2D(0.0, screenWidth, 0.0, screenHeight);
        gl.glOrtho(0.0, screenWidth, 0.0, screenHeight, -1, 1);
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();

        //glColor3f(1.0, 1.0, 1.0);

        int points = 4;

        // Преобразование к пикселям
        Polygon pp = p;
        for (int i = 0; i < points; i++) {
            Vector3d vectors = pp.getVector(i);
            vectors.setX((vectors.getX() * 0.5f + .5f) * screenWidth);
            vectors.setY((vectors.getY() * 0.5f + .5f) * screenHeight);
            vectors.setZ(vectors.getZ() + 1000.f);

            if (vectors.getX() >= screenWidth || vectors.getX() < 0) return;
            if (vectors.getY() >= screenHeight || vectors.getY() < 0) return;
        }

        // Создаём таблицу рёбер ТР
        Edge[] TP = new Edge[screenHeight];
        for (int i = 0; i < screenHeight; i++) TP[i] = null;

        // Вставляем рёбра в ТР
        for (int i = 0; i < points; i++) {
            // Вычисление структуры ребра
            Vector3d first = pp.getVector(i);
            Vector3d second = pp.getVector((i == points - 1) ? 0 : i + 1);

            // Вычисляем смещения и начальные положения по x и z
            int miny, maxy;
            float x1, x2;
            float z1, z2;

            if (first.getY() < second.getY()) {
                miny = Math.round(first.getY());
                maxy = Math.round(second.getY());
                x1 = first.getX();
                x2 = second.getX();
                z1 = first.getZ();
                z2 = second.getZ();
            } else {
                miny = (int) second.getY();
                maxy = (int) first.getY();
                x1 = second.getX();
                x2 = first.getX();
                z1 = second.getZ();
                z2 = first.getZ();
            }

            float dy = (float) maxy - miny;

            float dx = x2 - x1;
            float m1 = 1 / (dy / dx); // приращение по x

            float dz = z2 - z1;
            float m1z = 1 / (dy / dz); // приращение по z

            // Укорачивание
            if (miny != maxy) {
                maxy--;
            }

            Edge edge = new Edge(maxy, x1, m1, z1, m1z);
            // Вставка ребра в TP
            TP[miny] = tapInsertEdge(TP[miny], edge);
        }


        Edge tap = null;
        int y;

        // Заносим в y первую группу
        for (y = 0; y < screenHeight && TP[y] == null; y++) ;

        // Алгоритм построчного сканирования
        for (; y < screenHeight; y++) {
            // Добавить линии в tap
            for (Edge e = TP[y]; e != null; ) {
                Edge nexte = e.getNext();
                tap = tapInsertEdge(tap, e);
                e = nexte;
            }

            // Распечатать точки

            if (tap != null) {
                // Edge *edge
                for (Edge edge = tap; edge != null; edge = edge.getNext().getNext()) {
                    if (edge.getNext() == null) break;

                    Edge edgeNext = edge.getNext();


                    if (edgeNext.getNext() != null && edgeNext.getNext().getNext() == null)
                        edgeNext = edgeNext.getNext();

                    // Рисуем палку
                    int fromx = (int) edge.getX();
                    int tox = (int) edgeNext.getX();
                    for (int x = fromx; x <= tox; x++) {
                        gl.glColor3f(lightI.getX(), lightI.getY(), lightI.getZ());
                        gl.glVertex2i(x, y);

                        int pointIndex = y * screenHeight * 4 + x * 4;

                        // Вычисление глубины текущей точки
                        float lineLength = tox - fromx;
                        float lineCurrentLength = x - fromx;
                        float zplace = lineCurrentLength / lineLength;
                        float depth = edge.getZ() + (edge.getNext().getZ() - edge.getZ()) * zplace;

                        // Запись в буфер всего кроме фона
                        if (depth > buffer[pointIndex + 3]) {
                            buffer[pointIndex] = lightI.getX();
                            buffer[pointIndex + 1] = lightI.getY();
                            buffer[pointIndex + 2] = lightI.getZ();
                            buffer[pointIndex + 3] = depth;
                        }

                    }
                }
            }

            // Удаление закончившихся линий
            Edge preve = tap, e = tap, nexte;
            while (e != null) {
                nexte = e.getNext();
                if (e.getMaxy() == y) {
                    if (e == tap)
                        tap = nexte;
                    else
                        preve.setNext(e.getNext());
                    e = null;
                } else {
                    preve = e;
                }
                e = nexte;
            }

            // Смещаем сканирующую строку и приращиваем x
            for (Edge edge = tap; edge != null; edge = edge.getNext()) {
                edge.setX(edge.getX() + edge.getM1());
                edge.setZ(edge.getZ() + edge.getM1z());
            }

            // Пересортировываем сканирующую строку
            tap = tapSort(tap);
        }

        // Очищаем если остались линии
        tapClear(tap);
    }

    @Override
    public void init(GLAutoDrawable drawable) {

    }

    @Override
    public void dispose(GLAutoDrawable drawable) {

    }

    // Рендеринг кадра
    @Override
    public void display(GLAutoDrawable drawable) {

        gl = drawable.getGL().getGL2();
        // Генерируем фигуру
        polygons = generateFigure(objectSize, objectStep, objectStep);

        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();
        gl.glMatrixMode(GL_MODELVIEW);
        gl.glLoadIdentity();

        gl.glClear(GL_COLOR_BUFFER_BIT);

        // Выбор цвета
        gl.glColor3f(1.0f, 1.0f, 1.0f);

        View view;

        switch (scene) {
            case '1': { // аксанометрия wired
                view = new View(cameraHorizontalAngle, cameraVerticalAngle, 999999999.f, 0.f);
                drawWiredObject(view);
                break;
            }
            case '2': {  // перспектива wired
                view = new View(cameraHorizontalAngle, cameraVerticalAngle, 4.f, 1.f);
                drawWiredObject(view);
                break;
            }
            case '3': { // аксанометрия wired without
                view = new View(cameraHorizontalAngle, cameraVerticalAngle, 999999999.f, 0.f);
                drawSolidWiredObject(view);
                break;
            }
            case '4': {  // перспектива wired without
                view = new View(cameraHorizontalAngle, cameraVerticalAngle, 4.f, 1.f);
                drawSolidWiredObject(view);
                break;
            }
            case '5': { // аксанометрия scanline
                view = new View(cameraHorizontalAngle, cameraVerticalAngle, 999999999.f, 0.f);
                drawSolidObject(view);
                break;
            }
            case '6': { // перспектива scanline
                view = new View(cameraHorizontalAngle, cameraVerticalAngle, 4.f, 1.f);
                drawSolidObject(view);
                break;
            }
        }

        gl.glFlush();
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

    }

    @Override
    public void keyTyped(KeyEvent e) {
        System.out.println("Key event" + e);
        scene = e.getKeyChar();
    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
