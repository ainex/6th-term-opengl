package nstu.ulyanova.sixthterm.opengl.controlwork;

import java.util.Random;

public final class Utils {
    private Utils() {
    }

    static private Random random = new Random();


    // Случайное число
    public static float randomFloat(float from, float to) {
        return random.nextFloat();
    }

    // Максимальное значение
    public static float max(float a, float b) {
        return (a > b) ? a : b;
    }

    // Минимальное значение
    public static float min(float a, float b) {
        return (a > b) ? b : a;
    }

    // Косинус d (градусы)
    public static float cosd(float d) {
        return (float) Math.cos(Math.toRadians(d));
    }

    // Синус d (градусы)
    public static float sind(float d) {
        return (float) Math.sin(Math.toRadians(d));
    }
}
