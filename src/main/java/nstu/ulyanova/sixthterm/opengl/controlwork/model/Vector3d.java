package nstu.ulyanova.sixthterm.opengl.controlwork.model;

import nstu.ulyanova.sixthterm.opengl.controlwork.Utils;

// Вектор 3-х мерный
public class Vector3d {
    private float x;
    private float y;
    private float z;

    public Vector3d() {
    }

    public Vector3d(float x, float y) {
        this(x, y, 0.f);
    }

    public Vector3d(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // Повернуть себя
    void rotate(float a, float b) {
        float ca = Utils.cosd(a);
        float sa = Utils.sind(a);
        float cb = Utils.cosd(b);
        float sb = Utils.sind(b);
        Vector3d v = new Vector3d(
                x * ca - y * Utils.sind(a),
                x * sa * cb + y * ca * cb - z * sb,
                x * sa * sb + y * ca * sb + z * cb);
        x = v.x;
        y = v.y;
        z = v.z;
    }

    // Добавить перспективу
    public void withPerspective(float zk, float zp) {
        float zz = (zk - zp) / (zk - z);
        x = x * zz;
        y = y * zz;
        z = z - zp;
    }

    // Нормализовать
    public void normalize() {
        float d = (float) Math.sqrt(x * x + y * y + z * z);
        x /= d;
        y /= d;
        z /= d;
    }

    // Длина
    public float length() {
        return (float) Math.sqrt(x * x + y * y + z * z);
    }

    // Переопределение операции сложения
    public Vector3d add(final Vector3d v) {
        return new Vector3d(x + v.x, y + v.y, z + v.z);
    }

    // Переоаределение операции вычитания
    public Vector3d substact(final Vector3d v) {
        return new Vector3d(x - v.x, y - v.y, z - v.z);
    }

    // Переопределение операции скалярного произведения
    public float multiply(final Vector3d v) {
        return x * v.x + y * v.y + z * v.z;
    }

    // Переопределение операции произведения
    public Vector3d multyply(float v) {
        return new Vector3d(x * v, y * v, z * v);
    }

    // Переопределение операции деления
    public Vector3d devide(float v) {
        return new Vector3d(x / v, y / v, z / v);
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }
}
