package nstu.ulyanova.sixthterm.opengl.controlwork.model;

// 4-х векторный полигон
public class Polygon {
    private Vector3d[] vectors = new Vector3d[4]; // 4 точки в пространстве
    private Vector3d normal; // нормаль

    public Polygon() {
    }

    // Перемещение всех векторов полигона
    public void translate(final Vector3d v2) {
        vectors[0] = vectors[0].add(v2);
        vectors[1] = vectors[1].add(v2);
        vectors[2] = vectors[2].add(v2);
        vectors[3] = vectors[3].add(v2);
    }

    // Центр полигона
    public Vector3d center() {
        return (vectors[0].add(vectors[1]).add(vectors[2]).add(vectors[3])).multyply(0.25f);
    }


    // Поворот всех векторов полигона
    public void rotate(float a, float b) {
        vectors[0].rotate(a, b);
        vectors[1].rotate(a, b);
        vectors[2].rotate(a, b);
        vectors[3].rotate(a, b);
    }

    // Поворот всех векторов полигона
    public void withPerspective(float zk, float zp) {
        vectors[0].withPerspective(zk, zp);
        vectors[1].withPerspective(zk, zp);
        vectors[2].withPerspective(zk, zp);
        vectors[3].withPerspective(zk, zp);
    }

    public Vector3d[] getVectors() {
        return vectors;
    }

    public Vector3d getVector(int index) {
        return vectors[index];
    }

    public void setVector(Vector3d vector3d, int index) {
        vectors[index] = vector3d;
    }

    public void setVectors(Vector3d[] vectors) {
        this.vectors = vectors;
    }

    public Vector3d getNormal() {
        return normal;
    }

    public void setNormal(Vector3d normal) {
        this.normal = normal;
    }
}
