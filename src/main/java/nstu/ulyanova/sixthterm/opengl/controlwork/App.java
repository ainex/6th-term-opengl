package nstu.ulyanova.sixthterm.opengl.controlwork;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The main class. Creates Canvas with {@link Renderer}.
 */
public class App {
    private static final int SIZE = 300;
    private static final String CONTROL_WORK = "Control work";

    public static void main(String[] args) {

        //getting the capabilities object of GL2 profile
        final GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);

//		capabilities.setDepthBits(16);24

        // The canvas
        final GLCanvas glcanvas = new GLCanvas(capabilities);
        EllipseShape shape = new EllipseShape();
        glcanvas.addGLEventListener(shape);
        glcanvas.setSize(SIZE, SIZE);

        glcanvas.addKeyListener(shape);

        //creating frame
        final JFrame frame = new JFrame("Ellipse");

        //adding canvas to it
        frame.getContentPane().add(glcanvas);
        frame.setSize(frame.getContentPane().getPreferredSize());
        frame.setVisible(true);

        //add event listener to exit when window is closed
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }
}
