package nstu.ulyanova.sixthterm.opengl.lab03;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The main class. Creates Canvas with {@link Renderer}.
 */
public class App {
	private static final int SIZE = 800;
	private static final String LAB_03_BASIC_SHAPES = "Lab 03: Basic shapes";

	public static void main(String[] args) {

		//getting the capabilities object of GL2 profile
		final GLProfile profile  = GLProfile.get(GLProfile.GL2 );
		GLCapabilities capabilities  = new GLCapabilities( profile );

		// The canvas
		final GLCanvas glcanvas = new GLCanvas( capabilities);
		Renderer renderer = new Renderer();
		glcanvas.addGLEventListener( renderer );
		glcanvas.setSize( 400, 400 );


		glcanvas.addKeyListener(renderer);
//		glcanvas.addMouseListener(listener);
//		glcanvas.addMouseMotionListener(listener);

		// creating frame
		final JFrame frame = new JFrame ("Rotating Triangle");

		// adding canvas to it
		frame.getContentPane().add( glcanvas );
		frame.setSize(frame.getContentPane() .getPreferredSize());
		frame.setVisible( true );

		//Instantiating and Initiating Animator
		final FPSAnimator animator = new FPSAnimator(glcanvas, 300,true);
		animator.start();

		//add event listener to exit when window is closed
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
}
