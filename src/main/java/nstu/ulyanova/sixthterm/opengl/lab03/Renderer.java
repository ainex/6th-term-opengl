package nstu.ulyanova.sixthterm.opengl.lab03;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.time.LocalTime;

/**
 * Displays lines, points, triangles, etc.
 */
public class Renderer implements GLEventListener, KeyListener {
	private float spin = 0.0f;
	private float rtri;  //for angle of rotation
	@Override
	public void init(GLAutoDrawable glAutoDrawable) {


		final GL2 gl = glAutoDrawable.getGL().getGL2();

		// Set "clearing" or background color
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Black and opaque
	}

	@Override
	public void dispose(GLAutoDrawable glAutoDrawable) {

	}

	@Override
	public void display(GLAutoDrawable glAutoDrawable) {
		final GL2 gl = glAutoDrawable.getGL().getGL2();
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		spinDisplay();
		gl.glPushMatrix();
		gl.glRotatef(spin, 0.0f, 0.0f, 1.0f);
		gl.glColor3f(1.0f, 1.0f, 1.0f);
		gl.glBegin(GL2.GL_POLYGON);
		gl.glVertex2f(-25.0f,-25.0f);
		gl.glVertex2f(25.0f,-25.0f);
		gl.glVertex2f(25.0f,25.0f);
		gl.glVertex2f(-25.0f,25.0f);
		gl.glEnd( );
		gl.glPopMatrix();
		//gl.swapBuffer()

	}

	@Override
	public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {
		final GL2 gl = glAutoDrawable.getGL().getGL2();
		gl.glViewport (0, 0,  i2, i3);
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrtho(-50.0, 50.0, -50.0, 50.0, -1.0, 1.0);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
		gl.glLoadIdentity();

	}

	@Override
	public void keyTyped(KeyEvent e) {
		System.out.println("Key event" + e);
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	private void spinDisplay() {
		spin = spin + 2.0f;
		if (spin > 360.0)
			spin = spin - 360.0f;
	}

}
