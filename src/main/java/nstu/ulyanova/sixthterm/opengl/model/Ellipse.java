package nstu.ulyanova.sixthterm.opengl.model;

import com.jogamp.graph.geom.SVertex;
import com.jogamp.graph.geom.Vertex;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import nstu.ulyanova.sixthterm.opengl.model.Polygon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.jogamp.opengl.GL.*;

/**
 * Displays ellipse and it's projections
 */
public class Ellipse implements GLEventListener {
    private GLU glu = new GLU();
    private Random random = new Random();

    @Override
    public void display(GLAutoDrawable drawable) {
        final GL2 gl = drawable.getGL().getGL2();
        // Clear the drawing area
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        // Reset the current matrix to the "identity"
        gl.glLoadIdentity();
//        gl.glDepthMask(true);

       // drawCenterPoint(gl);
        //gl.glColor3d(0, 1, 0);

        float dL = 5.0f;
        float dB = 5.0f;
        float radius = 0.5f;
//        gl.glBegin(GL2.GL_POLYGON); //cool!
//        gl.glBegin(GL2.GL_LINE_STRIP);
//        gl.glBegin(GL2.GL_POINTS);
        List<List<Vertex>>  vertexShape = new ArrayList<>();
        for (float L = 0; L < 360; L += dL) {
            List<Vertex> meridian = new ArrayList<>();
            for (float B = -90; B <= 90; B += dB) {
                float x = (float) (radius * Math.cos(Math.toRadians(B)) * Math.sin(Math.toRadians(L)));
                float y = (float) (radius * Math.cos(Math.toRadians(B)) * Math.cos(Math.toRadians(L)));
                float z = (float) (2 * radius * Math.sin(Math.toRadians(B)));
                Vertex vertex = new SVertex();
                vertex.setCoord(x, y, z);
                meridian.add(vertex);
               // draw(gl, x, y, z, 45f, 45f);
            }
            vertexShape.add(meridian);
        }

        List<Polygon> polygons = getPolygons(vertexShape);

        gl.glColor3d(0, 1, 0);
        gl.glBegin(GL_LINE_STRIP);
        polygons.stream().forEach(p -> {

            p.getVertices().stream().forEach(v-> draw(gl,v.getX(), v.getY(), v.getZ(), 45f, 45f ));

        });

       polygons.forEach(p -> {
           System.out.println(p.getVertices().size());
       });

        gl.glEnd();
        gl.glFlush();
        System.out.println("Meridians amount: " + vertexShape.size());
        System.out.println("Vertexes in meridian: " + vertexShape.get(0).size());
        System.out.println("Polygons: " + polygons.size());
    }

    private List<Polygon> getPolygons(List<List<Vertex>> vertexShape) {
        List<Polygon> polygons = new ArrayList<>();
        for(int i = 0; i < vertexShape.size(); i++){
            List<Vertex> meridian = vertexShape.get(i);
            List<Vertex> nextMeridian = vertexShape.get(i+1 % vertexShape.size()-1);

            for ( int j = 0; j < meridian.size(); j++){
                Polygon polygon = new Polygon();
                polygon.addVertices(meridian.get(j), nextMeridian.get(j), meridian.get(j+1 % meridian.size()-1), meridian.get(j+1 % meridian.size()-1));
                polygons.add(polygon);
            }
        }
        return polygons;
    }

    private void drawCenterPoint(GL2 gl) {
        gl.glPointSize(5);
        gl.glBegin(GL2.GL_POINTS);
        gl.glColor3d(1, 0, 0);
        gl.glVertex3f(0, 0, 0);
//        gl.glVertex3f(0, 0.5f, 0);
        gl.glEnd();
        gl.glPointSize(1);
    }

    private void drawPolygon(GL2 gl, Polygon polygon, float a, float b) {
        double radA = Math.toRadians(a);
        double radB = Math.toRadians(b);
        double cosA = Math.cos(radA);
        double sinA = Math.sin(radA);
        double cosB = Math.cos(radB);
        double sinB = Math.sin(radB);
        gl.glBegin(GL2.GL_POLYGON);
        polygon.getVertices().forEach(v -> {
            double x1 = v.getX() * cosA - v.getY() * sinA;
            double y1 = v.getX() * sinA * cosB + v.getY() * cosA * cosB - v.getZ() * sinB;
            double z1 = v.getX() * sinA * sinB + v.getY() * cosA * sinB + v.getZ() * cosB;
            gl.glVertex3d(x1, y1, z1);
        });
        gl.glEnd();
        gl.glFlush();
    }

    /**
     * x` = x*cosα - y*sinα
     * y` = x*sinα*cosβ + y*cosα*cosβ-z*sinβ
     * z` = x*sinα*sinβ + y*cosα*sinβ + z*cosβ
     */
    private void draw(GL2 gl, double x, double y, double z, float a, float b) {
        double cosA = Math.cos(Math.toRadians(a));
        double sinA = Math.sin(Math.toRadians(a));
        double cosB = Math.cos(Math.toRadians(b));
        double sinB = Math.sin(Math.toRadians(b));

        double x1 = x * cosA - y * sinA;
        double y1 = x * sinA * cosB + y * cosA * cosB - z * sinB;
        double z1 = x * sinA * sinB + y * cosA * sinB + z * cosB;
        gl.glVertex3d(x1, y1, z1);
    }

    @Override
    public void dispose(GLAutoDrawable arg0) {
        //method body
    }

    @Override
    public void init(GLAutoDrawable drawable) {
//        final GL2 gl = drawable.getGL().getGL2();
//        gl.glClearDepth(1.0f);
//        gl.glEnable(GL_DEPTH_TEST);
//        gl.glDepthFunc(GL_LEQUAL);
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

    }
}
