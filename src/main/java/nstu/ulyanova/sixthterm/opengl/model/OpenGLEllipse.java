package nstu.ulyanova.sixthterm.opengl.model;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;

import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_MODELVIEW;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;

/**
 * Displays ellipse and it's projections
 */
public class OpenGLEllipse implements GLEventListener {
	private GLU glu = new GLU();

	@Override
	public void display(GLAutoDrawable drawable) {

		final GL2 gl = drawable.getGL().getGL2();
		gl.glPointSize(5);
		gl.glBegin(GL2.GL_POINTS);
		gl.glColor3d(1, 0, 0);
		gl.glVertex3f(0, 0, 0);
		gl.glEnd();

		gl.glPointSize(1);
		gl.glColor3d(0, 1, 0);
		//produces the transfer of the object, adding the values of its parameters
//		gl.glTranslatef(0.0f, 0f, -5.0f);
//		gl.glRotatef(15,1.0f,0.0f,0.0f);
		float dL = 5.0f;
		float dB = 5.0f;
		float R = 4.0f;
		gl.glBegin(GL2.GL_LINE_STRIP);
		for (float L = 0; L < 360; L += dL)
			for (float B = -90; B <= 90; B += dB) {
				double x = R * Math.cos(Math.toRadians(B)) * Math.sin(Math.toRadians(L));
				double y = R * Math.cos(Math.toRadians(B)) * Math.cos(Math.toRadians(L));
				double z = 2*R * Math.sin(Math.toRadians(B));
				//(X, Y) = Преобразование координат (х, у, z);
				//Рисование отрезка до точки (X, Y);
				gl.glVertex3d(x, y, z);
			}
		gl.glEnd();
//		gl.glTranslatef(0.0f, 0f, -5.0f);
		gl.glRotatef(90,1.0f,0.0f,0.0f); //x-rotate
//		gl.glRotatef(90,0.0f,1.0f,0.0f); //y-rotation
	}

	@Override
	public void dispose(GLAutoDrawable arg0) {
		//method body
	}

	@Override
	public void init(GLAutoDrawable arg0) {
		// method body
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {

		GL2 gl = drawable.getGL().getGL2();
		if (height <= 0)
			height = 1;

		final float h = (float) width / (float) height;
//		gl.glViewport(0, 0, width, height);
//		gl.glMatrixMode(GL2.GL_PROJECTION);
//		gl.glLoadIdentity();

		//45 and more - the scene is fare
//		glu.gluPerspective(90.0f, h, 2.0, 10.0); //ok
//		glu.gluPerspective(90.0f, h, 2.0, 10.0); //ok
	//	gl.glOrtho(-5.0, 5.0, -5.0, 5.0, 0.0, 8.0); //no 3d mode
//		gl.glMatrixMode(GL_PROJECTION);
//		gl.glLoadIdentity();
//		gl.glMatrixMode(GL2.GL_PROJECTION);
//		gl.glLoadIdentity();

//		glu.gluLookAt(0.0f,0.0f,0.0f, 0.0f,0.0f,0.0f, 0.0f,1.0f,0.0f);

		// Select part of window.
		gl.glViewport(0, 0, width, height);
		// Set projection matrix.
		gl.glMatrixMode(GL_PROJECTION);
		//Load the identity matrix.
		gl.glLoadIdentity();
		float fovAngle = 90f;
		glu.gluPerspective(fovAngle, h, 2.0, 20.0);
		// Set camera.
		gl.glMatrixMode(GL_MODELVIEW);
		//Load the identity matrix.
		gl.glLoadIdentity();
		glu.gluLookAt(0, 0, -20.0, // Положение глаз, взгляд "из"
				0, 0, 0.0, // Цель, взгляд "на"
				0.0, 1.0, 0.0); // Пока игнорируем

	}


}
