package nstu.ulyanova.sixthterm.opengl.model;


import com.jogamp.graph.geom.Vertex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Polygon {

    private Vertex[] vertices;

    public Polygon() {
        this(4);
    }

    public Polygon(int verticesAmount) {
        vertices = new Vertex[verticesAmount];
    }

    public void addVertices(Vertex v0, Vertex v1, Vertex v2, Vertex v3) {
        vertices[0] = v0;
        vertices[1] = v1;
        vertices[2] = v2;
        vertices[3] = v3;
    }

    public List<Vertex> getVertices(){
        return new ArrayList<>(Arrays.asList(vertices));
    }

}
