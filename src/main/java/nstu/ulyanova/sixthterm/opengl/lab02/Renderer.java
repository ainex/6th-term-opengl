package nstu.ulyanova.sixthterm.opengl.lab02;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import java.time.LocalTime;

/**
 * Displays lines, points, triangles, etc.
 */
public class Renderer implements GLEventListener {
	@Override
	public void init(GLAutoDrawable glAutoDrawable) {
		final GL2 gl = glAutoDrawable.getGL().getGL2();

		// Set "clearing" or background color
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // Black and opaque
	}

	@Override
	public void dispose(GLAutoDrawable glAutoDrawable) {

	}

	@Override
	public void display(GLAutoDrawable glAutoDrawable) {
		final GL2 gl = glAutoDrawable.getGL().getGL2();
		//Clear the color buffer with current clearing color
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);

		//Line
		gl.glBegin(GL2.GL_LINES);
		gl.glColor3d(0, 0, 1);
		gl.glVertex3f(0, -0.50f, 0);
		gl.glColor3d(1, 0, 0);
		gl.glVertex3f(0, 0.50f, 0);
		gl.glEnd();
		System.out.println("in display " + LocalTime.now());

		//Points
		gl.glPointSize(5);
		gl.glBegin(GL2.GL_POINTS);
		gl.glColor3d(1, 0, 0);
		gl.glVertex3f(0.0f, 0.5f, 0); // first point
		gl.glColor3d(0, 1, 0);
		gl.glVertex3f(-1.0f, 1.0f, 0);   // second point
		gl.glColor3d(0, 0, 1);
		gl.glVertex3f(0.0f, -0.5f, 0); // third point
		gl.glEnd();

		//Triangle
		gl.glBegin(GL2.GL_TRIANGLES);
		gl.glColor3f(1.0f, 0.0f, 0.0f); // Red
		gl.glVertex2f(0.3f, -0.4f);
		gl.glColor3f(0.0f, 1.0f, 0.0f); // Green
		gl.glVertex2f(0.9f, -0.4f);
		gl.glColor3f(0.0f, 0.0f, 1.0f); // Blue
		gl.glVertex2f(0.6f, -0.9f);
		gl.glEnd();

		// Form a closed polygon
		gl.glBegin(GL2.GL_POLYGON);
		gl.glColor3f(1.0f, 1.0f, 1.0f); // White
		gl.glVertex2f(0.4f, 0.2f);
		gl.glVertex2f(0.6f, 0.2f);
		gl.glVertex2f(0.7f, 0.4f);
		gl.glVertex2f(0.6f, 0.6f);
		gl.glVertex2f(0.4f, 0.6f);
		gl.glVertex2f(0.3f, 0.4f);
		gl.glEnd();

		//Form a quad
		gl.glBegin(GL2.GL_QUADS);
		gl.glColor3f(1.0f, 0.0f, 0.0f); // Red
		gl.glVertex2f(-0.8f, 0.1f);
		gl.glVertex2f(-0.2f, 0.1f);
		gl.glVertex2f(-0.2f, 0.7f);
		gl.glVertex2f(-0.8f, 0.7f);
		gl.glEnd();

		gl.glFlush();
	}

	@Override
	public void reshape(GLAutoDrawable glAutoDrawable, int i, int i1, int i2, int i3) {

	}
}
