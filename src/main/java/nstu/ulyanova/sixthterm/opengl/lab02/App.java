package nstu.ulyanova.sixthterm.opengl.lab02;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * The main class. Creates Canvas with {@link Renderer}.
 */
public class App {
	private static final int SIZE = 800;
	private static final String LAB_02_BASIC_SHAPES = "Lab 02: Basic shapes";

	public static void main(String[] args) {
		//getting the capabilities object of GL2 profile
		final GLProfile profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities capabilities = new GLCapabilities(profile);

		// The canvas
		final GLCanvas canvas = new GLCanvas(capabilities);
		Renderer renderer = new Renderer();
		canvas.addGLEventListener(renderer);
		canvas.setSize(SIZE, SIZE);

		//creating frame
		final JFrame frame = new JFrame(LAB_02_BASIC_SHAPES);

		//adding canvas to frame
		frame.getContentPane().add(canvas);
		frame.setSize(frame.getContentPane().getPreferredSize());
		frame.setVisible(true);

		//add event listener to exit when window is closed
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}
}
